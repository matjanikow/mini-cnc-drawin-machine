#include <AFMotor.h>
AF_Stepper motor1(200, 1);
AF_Stepper motor1(200, 2);
void setup() {
Serial.begin(9600); // set up Serial library at 9600 bps
Serial.println("Stepper test!");
motor1.setSpeed(50); // 10 rpm
motor1.step(400, FORWARD, SINGLE);
motor1.release();
delay(1000);
motor2.setSpeed(50); // 10 rpm
motor2.step(400, FORWARD, SINGLE);
motor2.release();
}
void loop() {
motor1.step(200, FORWARD, SINGLE);
motor1.step(200, BACKWARD, SINGLE);
motor1.step(200, FORWARD, DOUBLE);
motor1.step(200, BACKWARD, DOUBLE);
motor1.step(200, FORWARD, INTERLEAVE);
motor1.step(200, BACKWARD, INTERLEAVE);
motor1.step(200, FORWARD, MICROSTEP);
motor1.step(200, BACKWARD, MICROSTEP);

motor2.step(200, FORWARD, SINGLE);
motor2.step(200, BACKWARD, SINGLE);
motor2.step(200, FORWARD, DOUBLE);
motor2.step(200, BACKWARD, DOUBLE);
motor2.step(200, FORWARD, INTERLEAVE);
motor2.step(200, BACKWARD, INTERLEAVE);
motor2.step(200, FORWARD, MICROSTEP);
motor2.step(200, BACKWARD, MICROSTEP);
}