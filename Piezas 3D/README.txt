                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1372864
Mini Traceur Arduino - Mini CNC Plotter by projetsdiy is licensed under the Creative Commons - Attribution - Non-Commercial - Share Alike license.
http://creativecommons.org/licenses/by-nc-sa/3.0/

# Summary

Eléments de structure pour réaliser un mini traceur (CNC Plotter) en recyclant 2 anciens lecteurs de CD/DVD.

Structural elements to build a Mini CNC Plotter based on Arduino and 2 old CD / DVD.

Toutes les étapes pour la fabrication à retrouver sur cette page http://www.projetsdiy.fr/recycler-lecteurs-dvd-mini-traceur-cnc-plotter-arduino/

A tutorial with all steps you need to follow  
http://www.projetsdiy.fr/mini-cnc-plotter-arduino-l293d-stepper-motor/



# Print Settings

Printer: Dagoma discovery200
Rafts: Doesn't Matter
Supports: Doesn't Matter