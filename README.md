# Mini Cnc Drawin Machine de Materiales reciclados

Este repositorio recopila varios projectos algunos editados por mi para poder crear una maquina de cnc para dibujo.


Requerimentos:

- 8 cables
- Arduino (en este caso arduino UNO)
- Stepper Motor shield (o en su defecto se puede armar con 2 L293D)
- 2x DVD/CD Drives
- Mini Servo Motor
- Una pieza de algún material rigido (sintra, madera, etc) de 20x16 cm para el eje X 
- 2 piezas duras (madera/mensulas de metal) de 14x4 cm para el eje Y
- Destornillador
- Soldador de estaño
- Pegamento
- Transformador de 5v
![](Images/IMG_20190604_235009.jpg)

<h4>Paso 1</h4>
Desarmar 2 lectoras de cd/dvd:
![](Images/IMG_20190612_201113.jpg)
<br>
Una vez que tengamos los 2 esqueletos notarás que los motores paso a paso tienen 4 pines, primero debemos ver con un tester cual de estos 4 estan en serie. Una vez logrado eso soldamos 2 pares de cables por cada bobina
![](Images/IMG_20190617_202240.jpg)
Debería quedar algo así el resultado final:
![](Images/photo4988239133302433902.jpg)
<h4>Paso 2</h4>

Conectamos los cables al shield una vez hecho deberíamos tener la siguiente estructura:
![](Images/IMG_20190617_235826.jpg)
para probar ahora deberíamos entrar a la carpeta Test y probar el test en el arduino uno. El codigo es una version modificada por mi para que testee **AMBOS** ejes del siguiente elance:
[![](http://img.youtube.com/vi/cwwYBgH-w8A/0.jpg)](http://www.youtube.com/watch?v=cwwYBgH-w8A "Testear eje x")

###### **Importante:**
 Una vez que esté todo esto en marcha necesitamos un cargador de 5v para alimentar bien el shield, con esto podremos probar los 2 motores paso a paso y un servomotor
![](Images/IMG_20190628_012945.jpg)

<h4>Paso 3 (el eje y)</h4>

Una vez que decidamos cual eje va a ser el Y debemos darle una superficie para apoyar el papel a dibujar lo suficientemente alta para no chocar con el motor
para eso se debe armar una base
![](Images/IMG_20190709_153142.jpg)
![](Images/IMG_20190709_153212.jpg)
![](Images/IMG_20190709_153613.jpg)
![](Images/IMG_20190709_154050.jpg)
![](Images/IMG_20190709_154441.jpg)
![](Images/IMG_20190709_154039.jpg)

Luego ajustamos las piezas esquineras para empezar a montar el eje X
<h4>Paso 4 (el eje X)</h4>



Pegar la pieza impresa en la lectora del eje X:
![](Images/5281b6386f19d4964084c459d251a975_preview_featured.jpg)
luego con un resorte, una pieza metalica, un tornillo y una tuerca debemos armar la estructura de la lapicera:
![](Images/IMG_20190711_170615.jpg)

Una vez terminada la estructura deberíamos tener algo así:
![](Images/IMG_20190711_171058.jpg)


## SOFTWARE:

<h4>Paso 5 (Arduino)</h4>
En el arduino ide importamos la libreria (.zip) y corremos el codigo que están dentro de la carpeta 'Arduino Code'
![](Images/Arduino-IDE.jpg)
<h4>Paso 6 (Processing)</h4>
Luego abrimos processing (instalarlo si no lo tenemos) y correr el codigo de la carpeta "Processing"

<h4>Paso 7 (Gcodes)</h4>
Este es un video introductorio a los gcodes (son las coordenadas que vamos a imprimir)
[![](http://img.youtube.com/vi/EiEAuXtwgtw/0.jpg)](http://www.youtube.com/watch?v=EiEAuXtwgtw "Gcode")


<h4>Paso 8 (Gcodes)</h4>
Deberíamos tener todo andando
![](Images/t_video4988239132846194928.mp4)
![](Images/IMG_20190710_224557.jpg)


Inconvenientes:
-Si la máquina vibra y pero no anda, se debe invertir los cables de los ejes de un bobinado
-Si el processing dice que no tiene permisos para comunicarse con el Arduino hay que darle permisos
-Si funciona la máquina pero anda desprolija se tiene que ajustar el pen delay de 50 a 20